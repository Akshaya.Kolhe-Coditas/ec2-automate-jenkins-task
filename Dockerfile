# Use the official httpd image as the base image
FROM httpd:latest

# Copy your project files into the web server directory
COPY . /usr/local/apache2/htdocs/
